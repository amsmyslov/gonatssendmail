package main

import (
	"encoding/json"
	"fmt"
	"github.com/nats-io/nats.go"
	"log"
	"net/smtp"
	"os"
	"os/signal"
	"syscall"
)

type EmailMessage struct {
	Title    string    `json:"title"`
	Email string    `json:"email"`
	Body string `json:"body"`
}

func ProcessError(err error)  {
	if err != nil {
		log.Fatal(err)
	}
}

func main()  {
	nc, err := nats.Connect("nats://localhost:32773")
	ProcessError(err)
	defer nc.Close()

	done:=make(chan struct{},1)

	c := make(chan os.Signal)
	signal.Notify(c, os.Interrupt, syscall.SIGTERM)
	go func() {
		<-c
		os.Exit(1)
	}()

	nc.Subscribe("email", func(m *nats.Msg){
		var emailMessage EmailMessage

		err:=json.Unmarshal(m.Data,&emailMessage)
		ProcessError(err)

		from := "itpark.team@gmail.com"
		password := "newitpark32"

		to := []string{emailMessage.Email}

		host:="smtp.gmail.com"
		port:="587"

		message := []byte("To: "+emailMessage.Email+"\r\n" +
			"Subject: "+emailMessage.Title+"\r\n" +
			"\r\n" +
			emailMessage.Body+"\r\n")

		auth := smtp.PlainAuth("", from, password, host)

		err = smtp.SendMail(host+":"+port, auth, from, to, message)
		ProcessError(err)

		fmt.Println("EmailTo: ",emailMessage.Email," Title:", emailMessage.Title," Body", emailMessage.Body)
		fmt.Println("message send")


		done<-struct{}{}
	})

	select {
		case <-done:
			return
	}
}