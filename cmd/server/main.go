package main

import (
	"github.com/gin-gonic/gin"
	"github.com/nats-io/nats.go"
	"io/ioutil"
	"log"
	"net/http"
)

func ProcessError(err error)  {
	if err != nil {
		log.Fatal(err)
	}
}

func main()  {

	nc, err := nats.Connect("nats://localhost:32773")
	ProcessError(err)
	defer nc.Close()

	router := gin.Default()
	router.POST("/email/send", func(c *gin.Context) {
		body,err:=ioutil.ReadAll(c.Request.Body)

		ProcessError(err)

		c.JSON(http.StatusOK,"email sended")

		nc.Publish("email",body)
	})

	router.Run() // listen and serve on 0.0.0.0:8080
}
